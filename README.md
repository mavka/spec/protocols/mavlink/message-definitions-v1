MAVLink Message Definitions v1.0
================================

Contains [Protobuf](https://protobuf.dev/) specification for MAVLink
[message definitions v1.0](https://github.com/mavlink/mavlink/tree/master/message_definitions/v1.0).

Proto definitions are placed in [`mavka/protocols/mavlink/message_definitions_v1/spec/v1alpha1`](mavka/protocols/mavlink/message_definitions_v1/spec/v1alpha1).
