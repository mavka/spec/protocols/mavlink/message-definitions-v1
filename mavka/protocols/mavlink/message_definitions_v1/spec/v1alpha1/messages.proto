syntax = "proto3";
package mavka.protocols.mavlink.message_definitions_v1.spec.v1alpha1;

// MAVLink protocol
//
// Protocol is a collection of dialects ([`Dialect`]).
message Protocol {
    // Map of dialects: `name` (`string`) => [`Dialect`].
    map<string, Dialect> dialects = 1;
}

// MAVLink dialect definition
//
// See: MAVLink [dialect schema](https://mavlink.io/en/guide/xml_schema.html).
message Dialect {
    // Dialect name.
    string name = 1;
    // The minor version number for the release, as included in the HEARTBEAT mavlink_version field.
    optional uint32 version = 2;
    // Unique dialect ID.
    optional uint32 dialect = 3;

    // Map of messages: `id` (`ui32`) => [`Message`].
    map<uint32, Message> messages = 100;
    // Map of enums: `name` (`string`) => [`Enum`].
    map <string, Enum> enums = 101;
}

// MAVLink message definition.
//
// See: [message](https://mavlink.io/en/guide/xml_schema.html#messages) section in MAVLink XML schema documentation.
message Message {
    // Unique message ID.
    uint32 id = 1;
    // Message name (expected to be unique).
    string name = 2;
    // Message description.
    string description = 3;

    // List of message fields.
    repeated MessageField fields = 100;

    // Message work-in-progress flag.
    //
    // See: MAVLink XML schema [WIP section](https://mavlink.io/en/guide/xml_schema.html#wip)
    bool wip = 200;
    // Deprecation status.
    optional Deprecated deprecated = 201;

    // Dialect where this message was defined
    optional string defined_in = 1000;
}

// Message field.
//
// Used in [`Message`].
//
// See: [message](https://mavlink.io/en/guide/xml_schema.html#messages) section in MAVLink XML schema documentation.
message MessageField {
    // Field name.
    string name = 1;
    // Description.
    string description = 2;
    // Data type of the message field.
    MavType type = 3;

    // Optional [`Enum`] name which defines message values.
    optional string enum = 100;

    // Units of measurement.
    optional Units units = 200;
    // Should be set to `true` for bitmask fields, default `false`
    bool bitmask = 201;
    // Print format.
    //
    // Uses C `printf`-like format (i.e. `0x%04x`).
    optional string print_format = 202;

    // Specification for default value.
    optional Value default = 300;
    // Specification for invalid value.
    //
    // Specifies a value that can be set on a field to indicate that the data is invalid: the recipient should ignore
    // the field if it has this value. For example, `BATTERY_STATUS.current_battery` specifies `invalid="-1"`, so a
    // battery that does not measure supplied current should set `BATTERY_STATUS.current_battery` to `-1`.
    optional MessageFieldInvalidValue invalid = 301;

    // Instance flag.
    //
    // If `true`, this indicates that the message contains the information for a particular sensor or battery (e.g.
    // `Battery 1`, `Battery 2`, etc.) and that this field indicates which sensor. Default is `false`.
    //
    // See: [message definition](https://mavlink.io/en/guide/xml_schema.html#messages) in MAVLink XML schema.
    bool instance = 400;

    // Extension flag.
    //
    // Indicates that field is used in [MAVLink 2](https://mavlink.io/en/guide/mavlink_2.html) only
    bool extension = 500;
}

// Data type of a [`MessageField`].
//
// Similar to a field in a C struct - the size of the data required to store/represent the data type.
// Fields can be signed/unsigned integers of size 8, 16, 23, 64 bits, single/double precision
// [IEEE754](https://en.wikipedia.org/wiki/IEEE_754) floating point numbers. They can also be arrays of these scalar
// types.
//
// > **Note!** We've intentionally excluded outdated `array[*]` type as it is not used in modern message definitions.
// > See MAVLink message definitions
// > [XML schema](https://github.com/ArduPilot/pymavlink/blob/master/generator/mavschema.xsd).
message MavType {
    // Field type
    oneof type {
        // Int8-based type.
        Int8 int8 = 1;
        // Int16-based type.
        Int16 int16 = 2;
        // Int32-based type.
        Int32 int32 = 3;
        // Int64-based type.
        Int64 int64 = 4;
        // UInt8-based type.
        UInt8 u_int8 = 5;
        // UInt16-based type.
        UInt16 u_int16 = 6;
        // UInt32-based type.
        UInt32 u_int32 = 7;
        // UInt64-based type.
        UInt64 u_int64 = 8;
        // Float-based (single precision) type.
        Float float = 9;
        // Double-based (double precision) type.
        Double double = 10;
        // Char-based (byte-long) type.
        Char char = 11;
        // Special type used for MAVLink version.
        UInt8MavlinkVersion uint8_mavlink_version = 12;

        // Array
        Array array = 20;
    }

    // Int8-based type.
    message Int8 {}
    // Int16-based type.
    message Int16 {}
    // Int32-based type.
    message Int32 {}
    // Int64-based type.
    message Int64 {}
    // UInt8-based type.
    message UInt8 {}
    // UInt16-based type.
    message UInt16 {}
    // UInt32-based type.
    message UInt32 {}
    // UInt64-based type.
    message UInt64 {}
    // Float-based (single precision) type.
    message Float {}
    // Double-based (double precision) type.
    message Double {}
    // Char-based (byte-long) type.
    message Char {}
    // Special type used for MAVLink version.
    message UInt8MavlinkVersion {}
    // Array
    message Array {
        MavType type = 1;
        uint32 length = 2;
    }

}

// Units of measurement.
//
// Used in [`MessageField`].
//
// See: MAVLink XML [schema](https://github.com/ArduPilot/pymavlink/blob/master/generator/mavschema.xsd).
//
// For value spacing each unit section (like time of distance) starts with value which is a multiple oh hundred (i.e.
// 100, 200).
enum Units {
    // Unspecified
    UNITS_UNSPECIFIED = 0;

    // Time. Seconds: "s".
    UNITS_SECONDS = 100;
    // Time. Deciseconds (second / 10): "ds".
    UNITS_DECI_SECONDS = 101;
    // Time. Centiseconds (second / 100): "cs".
    UNITS_CENTI_SECONDS = 102;
    // Time. Milliseconds: "ms".
    UNITS_MILLI_SECONDS = 103;
    // Time. Microseconds: "us".
    UNITS_MICRO_SECONDS = 104;
    // Time. Nanosecond: "ns".
    UNITS_NANO_SECONDS = 105;
    // Time (frequency). Hertz: "Hz".
    UNITS_HERTZ = 106;
    // Time (frequency). Hertz: "MHz".
    UNITS_MEGA_HERTZ = 107;

    // Distance. Kilometres: "km".
    UNITS_KILO_METRES = 200;
    // Distance. Decametres (metre * 10): "dam".
    UNITS_DECA_METRES = 201;
    // Distance. Metes: "m".
    UNITS_METRES = 202;
    // Distance (velocity). Metres per second: "m/s".
    UNITS_METRES_PER_SECOND = 203;
    // Distance (acceleration). Metres pers second squared: "m/s/s".
    UNITS_METRES_PER_SECOND_SQUARED = 204;
    // Distance (velocity). Five metres per second: "m/s*5".
    UNITS_FIVE_METRES_PER_SECOND = 205;
    // Distance. Decimetres: "dm".
    UNITS_DECI_METRES = 206;
    // Distance (velocity). Decimetres per second: "dm/s".
    UNITS_DECI_METRES_PER_SECOND = 207;
    // Distance. Centimetres: "cm".
    UNITS_CENTI_METRES = 208;
    // Distance (surface). Square centimetre (typically used in variance): "cm^2".
    UNITS_SQUARE_CENTI_METRES = 209;
    // Distance (velocity). Centimetres per second: "cm/s".
    UNITS_CENTI_METRES_PER_SECOND = 210;
    // Distance. Millimetres: "mm".
    UNITS_MILLI_METRES = 211;
    // Distance (velocity). Millimetres per second: "mm/s".
    UNITS_MILLI_METRES_PER_SECOND = 212;
    // Distance (velocity). Millimetres: "mm/h".
    UNITS_MILLI_METRES_PER_HOUR = 213;

    // Temperature. Kelvins: "K".
    UNITS_KELVINS = 300;
    // Temperature. Degrees Celsius: "degC".
    UNITS_DEGREES_CELSIUS = 301;
    // Temperature. Centi degree Celsius (degree / 100): "cdegC".
    UNITS_CENTI_DEGREE_CELSIUS = 302;

    // Angle. Radians: "rad".
    UNITS_RADIANS = 400;
    // Angle (velocity). Radians per second: "rad/s".
    UNITS_RADIANS_PER_SECOND = 401;
    // Angle (velocity). Milliradians per second: "mrad/s".
    UNITS_MILLI_RADIANS_PER_SECOND = 402;
    // Angle. Degrees: "deg".
    UNITS_DEGREES = 403;
    // Angle. Half-degrees (degree / 2): "deg/2".
    UNITS_HALF_DEGREES = 404;
    // Angle (velocity). Degrees per second: "deg/s".
    UNITS_DEGREES_PER_SECOND = 405;
    // Angle. Centi degrees (degree / 100): "cdeg".
    UNITS_CENTI_DEGREES = 406;
    // Angle (velocity). Centi degrees (degree / 100) per second: "cdeg/s".
    UNITS_CENTI_DEGREES_PER_SECOND = 407;
    // Angle. Degrees / 10^5: "degE5".
    UNITS_DEGREES_E5 = 408;
    // Angle. Degrees / 10^7: "degE7".
    UNITS_DEGREES_E7 = 409;
    // Angle (velocity). Rotations per minute: "rpm".
    UNITS_ROTATIONS_PER_MINUTE = 410;

    // Electricity. Volt: "V".
    UNITS_VOLT = 500;
    // Electricity. Centi volt (Volt / 100): "cV".
    UNITS_CENTI_VOLT = 501;
    // Electricity. Milli-volt: "mV".
    UNITS_MILLI_VOLT = 502;
    // Electricity. Ampere: "A".
    UNITS_AMPERE = 503;
    // Electricity. Ampere hour: "Ah".
    UNITS_AMPERE_HOUR = 504;
    // Electricity. Centi Ampere (Ampere / 100): "cA".
    UNITS_CENTI_AMPERE = 505;
    // Electricity. Milli ampere: "mA".
    UNITS_MILLI_AMPERE = 506;
    // Electricity. Milli Ampere hour: "mAh".
    UNITS_MILLI_AMPERE_HOUR = 507;

    // Magnetism. Milli Tesla: "mT".
    UNITS_MILLI_TESLA = 600;
    // Magnetism. Gauss: "gauss".
    UNITS_GAUSS = 601;
    // Magnetism. Milli-gauss: "mgauss".
    UNITS_MILLI_GAUSS = 602;

    // Energy. Hecto Joule: "hJ".
    UNITS_HECTO_JOULE = 700;

    // Power. Watt: "W".
    UNITS_WATT = 800;

    // Force. Milli G: "mG".
    UNITS_MILLI_G = 900;

    // Mass. Grams: "g".
    UNITS_GRAMS = 1000;
    // Mass. Kilograms: "kg".
    UNITS_KILO_GRAMS = 1001;

    // Pressure. Pascal: "Pa".
    UNITS_PASCAL = 1100;
    // Pressure. Hectopascal: "hPa".
    UNITS_HECTO_PASCAL = 1101;
    // Pressure. Kilopascal: "kPa".
    UNITS_KILO_PASCAL = 1102;
    // Pressure. Millibar: "mbar".
    UNITS_MILLI_BAR = 1103;

    // Ratio. Percent: "%".
    UNITS_PERCENT = 1200;
    // Ratio. Decipercent (percent / 10): "d%".
    UNITS_DECI_PERCENT = 1201;
    // Ratio. Centipercent (percent / 100): "c%".
    UNITS_CENTI_PERCENT = 1202;
    // Ratio. Decibel: "dB".
    UNITS_DECI_BEL = 1203;
    // Ratio. Decibel milli-Wats: "dBm".
    UNITS_DECI_BEL_MILLI_WATS = 1204;

    // Digital. Kibibyte (1024 bytes): "KiB".
    UNITS_KIBI_BYTE = 1300;
    // Digital (throughput). Kibibyte (1024 bytes) per second: "KiB/s".
    UNITS_KIBI_BYTE_PER_SECOND = 1301;
    // Digital. Mebibyte (1024*1024 bytes): "MiB".
    UNITS_MEBI_BYTE = 1302;
    // Digital (throughput). Mebibyte (1024*1024 bytes) per second: "MiB/s".
    UNITS_MEBI_BYTE_PER_SECOND = 1303;
    // Digital. Bytes: "bytes".
    UNITS_BYTES = 1304;
    // Digital (throughput). Bytes per second: "bytes/s".
    UNITS_BYTES_PER_SECOND = 1305;
    // Digital (throughput). Bits per second: "bits/s".
    UNITS_BITS_PER_SECOND = 1306;
    // Digital. Pixels: "pix".
    UNITS_PIXELS = 1307;
    // Digital. Decipixels (pixel / 10): "dpix".
    UNITS_DECI_PIXELS = 1308;

    // Flow. Grams per minute: "g/min".
    UNITS_GRAMS_PER_MINUTE = 1400;
    // Flow. Cubic centimetres per minute: "cm^3/min".
    UNITS_CUBIC_CENTI_METRES_PER_MINUTE = 1401;

    // Volume. Cubic centimetres: "cm^3".
    UNITS_CUBIC_CENTI_METRES = 1500;
    // Volume. Litres: "l".
    UNITS_LITRES = 1501;
}

// Value specification.
//
// Used in [`MessageField`], [`EnumEntryMavCmdParam`], and [`MessageFieldInvalidValue`].
//
// See: [message](https://mavlink.io/en/guide/xml_schema.html#messages) section in MAVLink XML schema documentation.
message Value {
    // Default value specification.
    oneof value {
        // Integer value.
        int32 int8 = 1;
        int32 int16 = 2;
        int32 int32 = 3;
        int64 int64 = 4;
        uint32 u_int8 = 5;
        uint32 u_int16 = 6;
        uint32 u_int32 = 7;
        uint64 u_int64 = 8;
        float float = 9;
        double double = 10;
        uint32 char = 11;
        Max max = 12;
    }

    // Maximum value for the type.
    message Max {
        MavType type = 13;
    }
}

// Specification for the invalid value of a [`MessageField`].
message MessageFieldInvalidValue {
    oneof invalid {
        // Field has a specific value.
        Value value = 1;
        // All array items are set to a specific value.
        Value all_items = 2;
        // First array item is set to a specific value.
        Value first_item = 3;
        // Field is set to the value of the specified enum entry.
        EnumEntryValue enum_entry_value = 10;
    }

    // Field is set to the value of the specified enum entry.
    message EnumEntryValue {
        // Value that marks invalid data.
        string name = 1;
    }
}

// MAVLink enum
//
// MAVLink enum is a special field type. There are two types of enums:
// - **regular**: value specifies a particular enum option (entry)
// - **bitmask**: each bit signifies a particular flag
//
// Enum options (entries) and flags are specified by [`EnumEntry`].
//
// See: [enum](https://mavlink.io/en/guide/xml_schema.html#enum) section in MAVLink XML schema documentation.
message Enum {
    // Enum name.
    string name = 1;
    // Description.
    string description = 2;
    // Map of enum entries: `name` (string) => [`EnumEntry`].
    map <string, EnumEntry> entries = 100;
    // Bitmask flag, set to `true` if enum is a bitmask
    bool bitmask = 200;
    // Deprecation status.
    optional Deprecated deprecated = 300;
    // List of dialects where this enum was defined
    repeated string defined_in = 1000;
}

// [`Enum`] entry (value)
//
// See: [enum entry](https://mavlink.io/en/guide/xml_schema.html#entry) section in MAVLink XML schema documentation.
message EnumEntry {
    // Enum value which designates this entry.
    uint32 value = 1;
    // Entry name.
    string name = 2;
    // Description.
    string description = 3;

    // `MAV_CMD` flag.
    //
    // Flags like `hasLocation` or `isDestination`.
    //
    // Makes sense only in the context of MAVLink command enum (`MAV_CMD`).
    //
    // See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
    optional EnumEntryMavCmdFlags cmd_flags = 100;
    // List of enum entry parameters.
    //
    // Makes sense only in the context of MAVLink command enum (`MAV_CMD`).
    //
    // See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
    repeated EnumEntryMavCmdParam params = 101;

    // Enum entry work-in-progress flag.
    //
    // See: MAVLink XML schema [WIP](https://mavlink.io/en/guide/xml_schema.html#wip) section.
    bool wip = 200;
    // Deprecation status.
    //
    // See: MAVLink XML schema [deprecated](https://mavlink.io/en/guide/xml_schema.html#deprecated) section.
    optional Deprecated deprecated = 201;

    // Dialect where this enum entry was defined
    optional string defined_in = 1000;
}

// [`EnumEntry`] `MAV_CMD` flags.
//
// Contains flags like `hasLocation` or `isDestination`.
//
// Makes sense only in the context of MAVLink command enum (`MAV_CMD`).
//
// See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
message EnumEntryMavCmdFlags {
    // Has location.
    //
    // A boolean (default `true`) that provides a hint to a GCS that the entry should be displayed as a "standalone"
    // location - rather than as a destination on the flight path. Apply for MAV_CMDs that contain lat/lon/alt location
    // information in param 5, 6, and 7 values but which are not on the vehicle path.
    //
    // See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
    optional bool has_location = 1;
    // Is destination.
    //
    // A boolean (default `true`) that provides a hint to a GCS that the entry is a location that should be displayed as
    // a point on the flight path. Apply for MAV_CMD that contain lat/lon/alt location information in their param 5, 6,
    // and 7 values, and that set a path/destination.
    //
    // See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
    optional bool is_destination = 2;
    // Mission only.
    //
    // Apply with value `true` if the MAV_COMMAND only "makes sense" in a mission. For example, the fence mission
    // commands could not possibly be useful in a command.
    //
    // See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
    optional bool mission_only = 3;
}

// [`EnumEntry`] `MAV_CMD` parameter.
//
// Makes sense only in the context of MAVLink command enum (`MAV_CMD`).
//
// See: MAVLink [command details](https://mavlink.io/en/guide/xml_schema.html#MAV_CMD) in XML schema docs.
message EnumEntryMavCmdParam {
    // Unique index from (up to 7 parameters are supported).
    uint32 index = 1;
    // Description.
    string description = 2;

    // Display label.
    //
    // Display name to represent the parameter in a GCS or other UI. All words in label should be capitalised.
    optional string label = 100;
    // Units of measurement
    optional Units units = 101;
    // Name of the enum ([`Enum`]) containing possible values for the parameter (if applicable).
    optional string enum = 102;
    // Decimal places
    //
    // Hint to a UI about how many decimal places to use if the parameter value is displayed.
    optional uint32 decimal_places = 103;
    // Allowed increments for the parameter value.
    optional Value increment = 104;
    // Minimum value for param.
    optional Value min_value = 105;
    // Maximum value for the param.
    optional Value max_value = 106;

    // Reserved flag.
    //
    // Boolean indicating whether param is reserved for future use. Default is `false`.
    bool reserved = 200;
    // Default value.
    //
    // Default value for the param (primarily used for `reserved` params, where the value is `0` or `NaN`).
    optional Value default = 201;
}

// Deprecation status
//
// Applicable to [`Message`], [`Enum`], and [`EnumEntry`].
//
// See [deprecated](https://mavlink.io/en/guide/xml_schema.html#deprecated) in MAVLink XML schema.
message Deprecated {
    // Since when this entry was deprecated.
    DeprecatedSince since = 1;
    // Name of the entity which replaces the deprecated one.
    string replaced_by = 2;
}

// Defines when an entry was deprecated.
message DeprecatedSince {
    // Year as signed integer.
    int32 year = 1;
    // Month as unsigned integer.
    uint32 month = 2;
}
